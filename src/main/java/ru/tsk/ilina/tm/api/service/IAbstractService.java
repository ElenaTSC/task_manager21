package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.model.AbstractEntity;

import java.util.List;

public interface IAbstractService<E extends AbstractEntity> {

    List<E> findAll();

    E add(E entity);

    E findById(String id);

    E removeById(String id);

    E remove(E entity);

}
