package ru.tsk.ilina.tm.api.service;

public interface ILogService {

    public void info(final String message);

    public void debug(final String message);

    public void command(final String message);

    public void error(final Exception e);


}
