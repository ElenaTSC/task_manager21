package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.ICommandRepository;
import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.constant.ArgumentConst;
import ru.tsk.ilina.tm.constant.TerminalConst;
import ru.tsk.ilina.tm.model.Command;

import java.util.*;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return arguments.get(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommand() {
        return commands.values();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getCommandName() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public Collection<String> getCommandArg() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand argument : arguments.values()) {
            final String arg = argument.arg();
            if (arg == null || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

}
