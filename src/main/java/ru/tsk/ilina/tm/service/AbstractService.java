package ru.tsk.ilina.tm.service;

import ru.tsk.ilina.tm.api.repository.IAbstractBusinessRepository;
import ru.tsk.ilina.tm.api.repository.IAbstractRepository;
import ru.tsk.ilina.tm.api.service.IAbstractService;
import ru.tsk.ilina.tm.exception.empty.EmptyIdException;
import ru.tsk.ilina.tm.exception.empty.EmptyNameException;
import ru.tsk.ilina.tm.exception.empty.EmptyUserIdException;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.AbstractEntity;
import ru.tsk.ilina.tm.repository.AbstractRepository;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity, R extends IAbstractRepository<E>> implements IAbstractService<E> {

    protected final R repository;

    protected AbstractService(R repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public E add(E entity) {
        if (entity == null) throw new TaskNotFoundException();
        return repository.add(entity);
    }

    @Override
    public E findById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E removeById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Override
    public E remove(E entity) {
        if (entity == null) return null;
        return repository.remove(entity);
    }

}
