package ru.tsk.ilina.tm.command.project;

import ru.tsk.ilina.tm.command.AbstractProjectCommand;
import ru.tsk.ilina.tm.enumerated.Sort;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects;
        if (sort == null || sort.isEmpty()) projects = serviceLocator.getProjectService().findAll(userId);
        else {
            Sort sortValue = Sort.valueOf(sort);
            System.out.println(sortValue.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(userId, sortValue.getComparator());
        }
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
        System.out.println("[OK]");
    }

}
