package ru.tsk.ilina.tm.command.project;

import ru.tsk.ilina.tm.command.AbstractProjectCommand;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project by id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByID(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectService().removeByID(userId, id);
    }

}
