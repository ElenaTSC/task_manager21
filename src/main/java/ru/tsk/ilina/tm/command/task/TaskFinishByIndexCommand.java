package ru.tsk.ilina.tm.command.task;

import ru.tsk.ilina.tm.command.AbstractTaskCommand;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

public final class TaskFinishByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @Override
    public String description() {
        return "Finish task by index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().finishByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
    }

}
