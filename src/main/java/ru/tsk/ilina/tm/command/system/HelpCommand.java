package ru.tsk.ilina.tm.command.system;

import ru.tsk.ilina.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display list of commands";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommand()) {
            System.out.println(command.toString());
        }
    }

}
