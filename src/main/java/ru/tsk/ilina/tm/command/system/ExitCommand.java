package ru.tsk.ilina.tm.command.system;

import ru.tsk.ilina.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String description() {
        return "Close application";
    }

    @Override
    public String arg() {
        return "-e";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
