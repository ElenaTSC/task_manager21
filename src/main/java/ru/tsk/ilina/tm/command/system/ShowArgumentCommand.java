package ru.tsk.ilina.tm.command.system;

import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.model.Command;

public final class ShowArgumentCommand extends AbstractCommand {

    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String description() {
        return "-arg";
    }

    @Override
    public String arg() {
        return "Display list arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getArguments()) {
            System.out.println(command.arg().toString());
        }
    }

}
