package ru.tsk.ilina.tm.model;

import ru.tsk.ilina.tm.enumerated.Status;

import java.util.Date;

public abstract class AbstractBusinessEntity extends AbstractOwnerEntity {

    private String name = "";
    private String description = "";
    private Status status = Status.NOT_STARTED;
    private Date startDate;
    private Date finishDate;
    private Date createdDate = new Date();

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + " : " + name;
    }

}
